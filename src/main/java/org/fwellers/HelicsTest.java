package org.fwellers;

import com.java.helics.SWIGTYPE_p_void;
import com.java.helics.helics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HelicsTest {
    private static final Logger logger = LoggerFactory.getLogger(HelicsTest.class);

    public void run() {
        //String configName = "helics_configs/test.json";
        String configName = "helics_configs/multiAgentFederate.json";
        URL resource = getClass().getClassLoader().getResource(configName);
        if (resource == null) {
            logger.error("Error: Config resource {} was not found!", configName);
            return;
        }
        SWIGTYPE_p_void federate = helics.helicsCreateValueFederateFromConfig(resource.getFile());
        String federateName = helics.helicsFederateGetName(federate);
        logger.debug("Created Federate {}", federateName);

        // Create Publication pointers
        int pubCount = helics.helicsFederateGetPublicationCount(federate);
        logger.info("num publications: {}", pubCount);
        List<SWIGTYPE_p_void> pubPointers = new ArrayList<>();
        for (int i = 0; i < pubCount; i++) {
            SWIGTYPE_p_void swigtypePVoid = helics.helicsFederateGetPublicationByIndex(federate, i);
            pubPointers.add(swigtypePVoid);
            String s = helics.helicsEndpointGetName(swigtypePVoid);
            logger.info("Publication: {}", s);
        }

        // Create Subscription pointers
        int subCount = helics.helicsFederateGetInputCount(federate);
        logger.info("num subscriptions: {}", subCount);
        List<SWIGTYPE_p_void> subPointers = new ArrayList<>();
        for (int i = 0; i < subCount; i++) {
            SWIGTYPE_p_void swigtypePVoid = helics.helicsFederateGetInputByIndex(federate, i);
            double d = 5;
            helics.helicsInputSetDefaultDouble(swigtypePVoid, d);
            String s = helics.helicsSubscriptionGetTarget(swigtypePVoid);
            subPointers.add(swigtypePVoid);
            logger.info("Subscriptions: {}", s);
        }

        // Enter execution mode
        helics.helicsFederateEnterInitializingMode(federate);
        helics.helicsFederateEnterExecutingMode(federate);

        int hours = 24;
        int totalInterval = 60 * 60 * hours;
        int checkInterval = 5 * 60;
        double grantedTime = -1;
        double currentTime = 0;

        while (currentTime < totalInterval) {
            currentTime += checkInterval;


            // publications
            while (grantedTime < currentTime) {
                grantedTime = helics.helicsFederateRequestTime(federate, currentTime);
            }

            for (SWIGTYPE_p_void subPointer : subPointers) {
                double[] a = new double[1];
                double[] b = new double[1];
                /*helics.helicsInputGetComplex(subPointer,a, b);
                logger.info("Load from distribution system: {} + {}^i", a[0], b[0]);*/
                double v = helics.helicsInputGetDouble(subPointer);
                String subName = helics.helicsInputGetName(subPointer);
                logger.info("Subscription {} received value {}", subName, v);
            }
            for (SWIGTYPE_p_void pubPointer : pubPointers) {
                //helics.helicsPublicationPublishComplex(pubPointer, 100000, 100000);
                helics.helicsPublicationPublishDouble(pubPointer, 70);
            }
        }
        helics.helicsFederateDisconnect(federate);
    }
}
