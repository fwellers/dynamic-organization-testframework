package org.fwellers.agents;

import jadex.core.IComponent;
import jadex.future.DefaultResultListener;
import jadex.future.IFuture;
import jadex.future.IntermediateDefaultResultListener;
import jadex.micro.annotation.Agent;
import jadex.model.annotation.OnStart;
import jadex.requiredservice.annotation.OnService;
import org.fwellers.communication.CommunicationService;
import org.fwellers.configuration.AgentConfiguration;
import org.fwellers.configuration.HelicsMapping;
import org.fwellers.helics_layer.*;
import org.fwellers.roles.FeederRole;
import org.fwellers.roles.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

@Agent
public class FeederAgent {
    private static final Logger logger = LoggerFactory.getLogger(FeederAgent.class);
    private StepSchedulerService stepSchedulerService;
    private EnvironmentService environmentService;
    private CommunicationService communicationService;

    private final int id;
    private AgentConfiguration agentConfiguration;
    private Role<?> role;

    private String totalLoadKey;
    private double totalLoad;

    public FeederAgent(int id, AgentConfiguration agentConfiguration) {
        this.id = id;
        this.agentConfiguration = agentConfiguration;
        for (HelicsMapping subscriptionTarget : agentConfiguration.subscriptionTargets()) {
            // todo: enable support for multiple properties
            totalLoadKey = subscriptionTarget.target();
        }
    }

    @Agent
    private IComponent agent;
    @OnService(name = "stepSchedulerService")
    public void registerStepSchedulerService(StepSchedulerService stepSchedulerService) {
        this.stepSchedulerService = stepSchedulerService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "environmentService")
    public void registerEnvironmentService(EnvironmentService environmentService) {
        this.environmentService = environmentService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "communicationService")
    public void registerCommunicationService(CommunicationService communicationService) {
        this.communicationService = communicationService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    private boolean allServicesRegistered() {
        return stepSchedulerService != null && environmentService != null && communicationService != null;
    }

    public void startAgent() {
        this.role = agentConfiguration.roleForConfig(id, environmentService, communicationService);
        logger.info("Agent execution started");
        IFuture<Collection<AgentSubscriberResponse>> subscribe = stepSchedulerService.subscribe(new DoubleAgentSubscriberInfo(totalLoadKey, Unit.V));
        subscribe.addResultListener(new IntermediateDefaultResultListener<>() {
            @Override
            public void intermediateResultAvailable(AgentSubscriberResponse result) {
                onStep(result);
            }
        });
    }

    public void onStep(AgentSubscriberResponse agentSubscriberResponse) {
        logger.info("Hello, Jadex! {} with step {} and value {}", this, agentSubscriberResponse.timestamp(), agentSubscriberResponse.getValue());
        if (totalLoadKey.equals(agentSubscriberResponse.getTarget())) {
            totalLoad = agentSubscriberResponse.getValue();
            role.setProperty(FeederRole.TOTAL_LOAD_PROPERTY, totalLoad);
        }
        if (role.isReady()) {
            role.execute();
        }
    }
}
