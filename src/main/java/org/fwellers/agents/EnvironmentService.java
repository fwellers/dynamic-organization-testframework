package org.fwellers.agents;

import jadex.providedservice.annotation.Service;

@Service
public interface EnvironmentService {
    double getFeederThreshold();
}
