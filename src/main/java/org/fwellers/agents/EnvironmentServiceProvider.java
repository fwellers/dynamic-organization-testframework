package org.fwellers.agents;

import jadex.micro.annotation.Agent;
import jadex.providedservice.annotation.Service;

@Agent
@Service
public class EnvironmentServiceProvider implements EnvironmentService {

    @Override
    public double getFeederThreshold() {
        return 45000;
    }
}
