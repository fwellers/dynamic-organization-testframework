package org.fwellers.agents;

import com.java.helics.helics;
import jadex.core.IComponent;
import jadex.micro.annotation.Agent;
import jadex.requiredservice.annotation.OnService;
import org.fwellers.communication.CommunicationService;
import org.fwellers.configuration.AgentConfiguration;
import org.fwellers.configuration.ConfigurationService;
import org.fwellers.helics_layer.HelicsFederateSchedulerServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Agent
public class MainSystemAgent {
    private static final Logger logger = LoggerFactory.getLogger(MainSystemAgent.class);
    private ConfigurationService configurationService;
    private EnvironmentService environmentService;
    private CommunicationService communicationService;


    @Agent
    private IComponent agent;

    @OnService(name = "configurationService")
    public void registerConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "environmentService")
    public void registerEnvironmentService(EnvironmentService environmentService) {
        this.environmentService = environmentService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "communicationService")
    public void registerCommunicationService(CommunicationService communicationService) {
        this.communicationService = communicationService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    private boolean allServicesRegistered() {
        return configurationService != null && environmentService != null && communicationService != null;
    }

    public void startAgent() {
        System.loadLibrary("helicsJava");
        if (logger.isDebugEnabled()) {
            logger.debug("Using helics with version: {}", helics.helicsGetVersion());
        }

        HelicsFederateSchedulerServiceImpl helicsFederateSchedulerService = new HelicsFederateSchedulerServiceImpl(configurationService.getAgents().size(), configurationService.totalPublications());
        IComponent.create(helicsFederateSchedulerService);

        List<AgentConfiguration> agents = configurationService.getAgents();
        for (int i = 0; i < agents.size(); i++) {
            AgentConfiguration agentConfiguration = agents.get(i);
            logger.info("Creating agent {} with role {}", agentConfiguration.name(), agentConfiguration.role());

            if ("feeder".equals(agentConfiguration.name())) {
                IComponent.create(new FeederAgent(i, agentConfiguration));
            } else {
                IComponent.create(new HouseAgent(i, agentConfiguration));
            }
        }
    }

}
