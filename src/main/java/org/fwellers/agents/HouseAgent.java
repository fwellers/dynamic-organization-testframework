package org.fwellers.agents;

import jadex.core.IComponent;
import jadex.future.*;
import jadex.micro.annotation.Agent;
import jadex.requiredservice.annotation.OnService;
import org.fwellers.communication.CommunicationService;
import org.fwellers.configuration.AgentConfiguration;
import org.fwellers.configuration.HelicsMapping;
import org.fwellers.helics_layer.*;
import org.fwellers.roles.AdaptiveHouseHeatingRole;
import org.fwellers.roles.ReTriggerEvent;
import org.fwellers.roles.Role;
import org.fwellers.roles.RoleListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Agent
public class HouseAgent implements RoleListener {
    private static final Logger logger = LoggerFactory.getLogger(HouseAgent.class);
    private StepSchedulerService stepSchedulerService;
    private EnvironmentService environmentService;
    private CommunicationService communicationService;

    private final int id;
    private AgentConfiguration agentConfiguration;
    private Role<?> role;

    private String airTemperatureKey;
    private double airTemperature;

    private AgentSubscriberResponse currentStep;

    @Agent
    private IComponent agent;

    public HouseAgent(int id, AgentConfiguration agentConfiguration) {
        this.id = id;
        this.agentConfiguration = agentConfiguration;
        for (HelicsMapping subscriptionTarget : agentConfiguration.subscriptionTargets()) {
            // todo: enable support for multiple properties
            airTemperatureKey = subscriptionTarget.target();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return agentConfiguration.name();
    }

    @OnService(name = "stepSchedulerService")
    public void registerStepSchedulerService(StepSchedulerService stepSchedulerService) {
        this.stepSchedulerService = stepSchedulerService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "environmentService")
    public void registerEnvironmentService(EnvironmentService environmentService) {
        this.environmentService = environmentService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    @OnService(name = "communicationService")
    public void registerCommunicationService(CommunicationService communicationService) {
        this.communicationService = communicationService;

        if (allServicesRegistered()) {
            startAgent();
        }
    }

    private boolean allServicesRegistered() {
        return stepSchedulerService != null && environmentService != null && communicationService != null;
    }

    public void startAgent() {
        role = agentConfiguration.roleForConfig(id, environmentService, communicationService);
        role.setListener(this);
        logger.info("Agent execution started");
        DoubleAgentSubscriberInfo agentSubscriberInfo = new DoubleAgentSubscriberInfo(airTemperatureKey, Unit.F);
        IntermediateFuture<AgentSubscriberResponse> subscribe = stepSchedulerService.subscribe(agentSubscriberInfo);
        subscribe.addResultListener(new IntermediateDefaultResultListener<>(){
            @Override
            public void intermediateResultAvailable(AgentSubscriberResponse result) {
                onStep(result);
            }
        });
    }

    @Override
    public IFuture<Void> onEvent(ReTriggerEvent event) {
        if (currentStep != null) {
            onStep(currentStep);
        }
        return IFuture.DONE;
    }

    public void onStep(AgentSubscriberResponse agentSubscriberResponse) {
        currentStep = agentSubscriberResponse;
        if (airTemperatureKey.equals(agentSubscriberResponse.getTarget()) && !role.isReady()) {
            logger.info("Hello, Jadex! {} with step {} and value {}", this, agentSubscriberResponse.timestamp(), agentSubscriberResponse.getValue());
            airTemperature = agentSubscriberResponse.getValue();
            role.setProperty(AdaptiveHouseHeatingRole.AIR_TEMPERATURE_PROPERTY, airTemperature);
        }
        if (role.isReady()) {
            List<AgentPublication> publicationList = role.execute();
            for (AgentPublication agentPublication : publicationList) {
                stepSchedulerService.sendResponse(agentPublication);
            }
        }
    }
}
