package org.fwellers.communication;

/**
 * Messages sent from the {@link org.fwellers.roles.FeederRole}.
 */
public enum FeederMessage implements Message {
    TOO_HIGH, OK;


    @Override
    public String toString() {
        switch (this) {
            case TOO_HIGH -> {
                return "TOO_HIGH";
            }
            default -> {
                return "OK";
            }
        }
    }
}
