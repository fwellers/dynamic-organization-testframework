package org.fwellers.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public record FilterValue(int branch, int meter) {
    private static final Logger logger = LoggerFactory.getLogger(FilterValue.class);
    public static FilterValue fromString(String string) {
        if (string.length() != 4) {
            logger.error("String has too short.");
        }
        char branchNum = string.charAt(1);
        char meterNum = string.charAt(3);
        return new FilterValue(branchNum, meterNum);
    }
}
