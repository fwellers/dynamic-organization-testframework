package org.fwellers.communication;

import jadex.future.IFuture;

import java.util.EventListener;

/**
 * EventListener Interface for message-based communication between agents.
 *
 * @param <T> The type of the implementing class.
 */
public interface MessageReceivedEventListener<T extends MessageReceivedEventListener<T>> extends EventListener {

    /**
     * Handler for receiving messages.
     *
     * @param messageEvent The message.
     */
    IFuture<Void> onMessage(MessageEvent<T> messageEvent);

    default IFuture<Void> onMessage(InitNegotiationBody<T> initNegotiationBody) {
        return onMessage((MessageEvent<T>) initNegotiationBody);
    }

    default IFuture<Void> onMessage(ProposeNegotiationBody<T, Double> proposeNegotiationBody) {
        return onMessage((MessageEvent<T>) proposeNegotiationBody);
    }

    default IFuture<Void> onMessage(FinishNegotiationBody<T> finishNegotiationBody) {
        return onMessage((MessageEvent<T>) finishNegotiationBody);
    }


    default boolean filterSet() {
        return false;
    }

    default boolean matches(FilterValue filter) {
        return true;
    }
}
