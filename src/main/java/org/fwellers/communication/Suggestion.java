package org.fwellers.communication;

public record Suggestion(String agentName, String property, double value, int time) implements Cloneable {


    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Suggestion(agentName, property, value, time);
    }}
