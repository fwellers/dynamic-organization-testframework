package org.fwellers.communication;

import org.jetbrains.annotations.Nullable;

public class ProposeNegotiationBody<T extends MessageReceivedEventListener<T>, E> extends MessageEvent<T> {
    private int time;

    public ProposeNegotiationBody(Object source, Class<T> targetClass, NegotiateActionMessage<E> message, int time, @Nullable String targetId) {
        super(source, targetClass, message, targetId);
        this.time = time;
    }

    public ProposeNegotiationBody(Object source, Class<T> targetClass, NegotiateActionMessage<E> message, int time) {
        super(source, targetClass, message);
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    @Override
    public NegotiateActionMessage<E> getMessage() {
        return (NegotiateActionMessage<E>) super.getMessage();
    }
}
