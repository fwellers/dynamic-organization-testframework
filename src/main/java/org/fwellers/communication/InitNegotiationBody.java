package org.fwellers.communication;

import org.jetbrains.annotations.Nullable;

public class InitNegotiationBody<T extends MessageReceivedEventListener<T>> extends MessageEvent<T> {
    private int maxParticipants;

    public InitNegotiationBody(Object source, Class<T> targetClass, NegotiationInitializeMessage message, @Nullable String targetId) {
        super(source, targetClass, message, targetId);
    }

    public InitNegotiationBody(Object source, Class<T> targetClass, NegotiationInitializeMessage message) {
        super(source, targetClass, message);
    }

    @Override
    public NegotiationInitializeMessage getMessage() {
        return (NegotiationInitializeMessage) super.getMessage();
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }
}
