package org.fwellers.communication;

public record NegotiationFinishMessage(String sourceAgentName, String propertyName) implements Message {
}
