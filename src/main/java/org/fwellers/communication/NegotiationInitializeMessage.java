package org.fwellers.communication;

public record NegotiationInitializeMessage(String agentName) implements Message {
}
