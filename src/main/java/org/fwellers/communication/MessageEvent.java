package org.fwellers.communication;

import org.jetbrains.annotations.Nullable;

import java.util.EventObject;
import java.util.Optional;

/**
 *
 * @param <T> The type of object/rule that is the target of the message
 */
public class MessageEvent<T extends MessageReceivedEventListener<T>> extends EventObject {
    private final transient Class<T> targetClass;
    private final Message message;
    @Nullable
    private final String targetId;

    public MessageEvent(Object source, Class<T> targetClass, Message message, @Nullable String targetId) {
        super(source);
        this.targetClass = targetClass;
        this.message = message;
        this.targetId = targetId;
    }

    public MessageEvent(Object source, Class<T> targetClass, Message message) {
        super(source);
        this.targetClass = targetClass;
        this.message = message;
        this.targetId = null;
    }

    public Optional<String> getTargetId() {
        return Optional.ofNullable(targetId);
    }

    public Optional<FilterValue> getFilter() {
        return getTargetId().map(FilterValue::fromString);
    }

    public Class<?> getSourceClass() {
        return source.getClass();
    }

    public Class<T> getTargetClass() {
        return targetClass;
    }

    public Message getMessage() {
        return message;
    }
}
