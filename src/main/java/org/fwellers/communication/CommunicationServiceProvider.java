package org.fwellers.communication;

import jadex.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.providedservice.annotation.Service;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Agent
public class CommunicationServiceProvider implements CommunicationService {
    private final Map<Class<? extends MessageReceivedEventListener>, List<MessageReceivedEventListener<? extends MessageReceivedEventListener>>> eventListenerMap;

    public CommunicationServiceProvider() {
        this.eventListenerMap = new HashMap<>();
    }

    @Override
    public <T extends MessageReceivedEventListener<T>> IFuture<Void> sendMessage(MessageEvent<T> messageEvent) throws NoListenerFoundException {
        // should be ok, key class should be same as value
        List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> listeners = eventListenerMap.get(messageEvent.getTargetClass());
        if (listeners == null) {
            throw new NoListenerFoundException();
        } else {
            getMatchingListeners(messageEvent, listeners).forEach(listener -> listener.onMessage(messageEvent));
        }
        return IFuture.DONE;
    }

    private static <T extends MessageReceivedEventListener<T>> List<MessageReceivedEventListener<T>> getMatchingListeners(MessageEvent<T> messageEvent, List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> listeners) {
        return listeners.stream()
                .map(listener -> (MessageReceivedEventListener<T>) listener)
                .filter(listener -> {
                    if (!listener.filterSet()) {
                        return true;
                    } else {
                        return messageEvent.getFilter()
                                .map(listener::matches)
                                .orElse(true);
                    }
                })
                //.filter(listener -> !listener.equals(messageEvent.getSource()))
                .toList();
    }

    @Override
    public <T extends MessageReceivedEventListener<T>> IFuture<Void> registerListener(MessageReceivedEventListener<T> eventListener) {
        List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> messageReceivedEventListeners
                = eventListenerMap.get(eventListener.getClass());
        if (messageReceivedEventListeners == null) {
            eventListenerMap.put(eventListener.getClass(), new ArrayList<>(List.of(eventListener)));
        } else {
            messageReceivedEventListeners.add(eventListener);
        }
        return IFuture.DONE;
    }

    @Override
    public <T extends MessageReceivedEventListener<T>> IFuture<Void> initNegotiation(InitNegotiationBody<T> initNegotiationBody) {
        List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> listeners = eventListenerMap.get(initNegotiationBody.getTargetClass());
        if (listeners == null) {
            throw new NoListenerFoundException();
        } else {
            List<MessageReceivedEventListener<T>> matchingListeners = getMatchingListeners(initNegotiationBody, listeners);
            initNegotiationBody.setMaxParticipants(matchingListeners.size());
            matchingListeners.forEach(listener -> listener.onMessage(initNegotiationBody));
        }
        return IFuture.DONE;
    }

    @Override
    public <T extends MessageReceivedEventListener<T>> IFuture<Void> proposeAction(ProposeNegotiationBody<T, Double> proposeNegotiationBody) {
        List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> listeners = eventListenerMap.get(proposeNegotiationBody.getTargetClass());
        if (listeners == null) {
            throw new NoListenerFoundException();
        } else {
            getMatchingListeners(proposeNegotiationBody, listeners).forEach(listener -> listener.onMessage(proposeNegotiationBody));
        }
        return IFuture.DONE;
    }

    @Override
    public <T extends MessageReceivedEventListener<T>> IFuture<Void> finishNegotiation(FinishNegotiationBody<T> finishedNegotiationBody) {
        List<MessageReceivedEventListener<? extends MessageReceivedEventListener>> listeners = eventListenerMap.get(finishedNegotiationBody.getTargetClass());
        if (listeners == null) {
            throw new NoListenerFoundException();
        } else {
            getMatchingListeners(finishedNegotiationBody, listeners).forEach(listener -> listener.onMessage(finishedNegotiationBody));
        }
        return IFuture.DONE;
    }
}
