package org.fwellers.communication;

import java.io.Serializable;

/**
 * Supertype for all Messages passed as part of a {@link MessageEvent}.
 */
public interface Message extends Serializable {
}
