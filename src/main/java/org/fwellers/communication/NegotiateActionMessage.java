package org.fwellers.communication;

public record NegotiateActionMessage<T>(int time, String sourceAgentName, String propertyName, T delta) implements Message {
}
