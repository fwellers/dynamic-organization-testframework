package org.fwellers.communication;

import jadex.future.IFuture;
import jadex.providedservice.annotation.Service;

@Service
public interface CommunicationService {
    <T extends MessageReceivedEventListener<T>> IFuture<Void> sendMessage(MessageEvent<T> messageEvent) throws NoListenerFoundException;

    <T extends MessageReceivedEventListener<T>> IFuture<Void> registerListener(MessageReceivedEventListener<T> eventListener);

    <T extends MessageReceivedEventListener<T>> IFuture<Void> initNegotiation(InitNegotiationBody<T> initNegotiationBody);

    <T extends MessageReceivedEventListener<T>> IFuture<Void> proposeAction(ProposeNegotiationBody<T, Double> proposeNegotiationBody);

    <T extends MessageReceivedEventListener<T>> IFuture<Void> finishNegotiation(FinishNegotiationBody<T> finishedNegotiationBody);
}
