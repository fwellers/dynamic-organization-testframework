package org.fwellers.communication;

import org.jetbrains.annotations.Nullable;

public class FinishNegotiationBody<T extends MessageReceivedEventListener<T>> extends MessageEvent<T> {
    public FinishNegotiationBody(Object source, Class<T> targetClass, NegotiationFinishMessage message, @Nullable String targetId) {
        super(source, targetClass, message, targetId);
    }

    public FinishNegotiationBody(Object source, Class<T> targetClass, NegotiationFinishMessage message) {
        super(source, targetClass, message);
    }

    @Override
    public NegotiationFinishMessage getMessage() {
        return (NegotiationFinishMessage) super.getMessage();
    }
}
