package org.fwellers.helics_layer;


import jadex.future.IFuture;
import jadex.future.IIntermediateFuture;
import jadex.future.IntermediateFuture;
import jadex.providedservice.annotation.Service;

import java.util.Collection;

/**
 *
 */
@Service
public interface StepSchedulerService {

    /**
     *
     * @param agentSubscriberInfo
     * @param <T>
     */
    <T>IntermediateFuture<AgentSubscriberResponse> subscribe(AgentSubscriberInfo<T> agentSubscriberInfo);

    /**
     * Warning: This method may not return! //todo: possibly change that
     * @param agentPublication
     */
    IFuture<Void> sendResponse(AgentPublication agentPublication);
}
