package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import org.jetbrains.annotations.Nullable;

import java.util.StringJoiner;

public abstract class AgentPublication implements Cloneable {
    protected int agentId;
    protected String fullTarget;
    protected String targetSimulation;
    @Nullable
    protected String targetAgent;
    protected String targetProperty;
    protected Unit targetUnit;

    public AgentPublication(int agentId, String target, Unit targetUnit) {
        this.agentId = agentId;
        this.targetUnit = targetUnit;
        String[] split = target.split("/");
        if (split.length == 3) {
            targetSimulation = split[0];
            targetAgent = split[1];
            targetProperty = split[2];
        }
        if (split.length == 2) {
            targetSimulation = split[0];
            targetProperty = split[1];
        }
        StringJoiner stringJoiner = new StringJoiner("/");
        stringJoiner.add(targetSimulation);
        if (targetAgent != null) {
            stringJoiner.add(targetAgent);
        }
        stringJoiner.add(targetProperty);
        fullTarget = stringJoiner.toString();
    }

    public String getFullTarget() {
        return fullTarget;
    }

    public String getTargetProperty() {
        return targetProperty;
    }

    public Unit getTargetUnit() {
        return targetUnit;
    }

    public abstract Object getValue();

    public abstract void setValue(Object value, Unit unit);

    public abstract boolean matchesPublication(String target);

    public abstract void publishValue(SWIGTYPE_p_void publicationPointer);

    @Override
    abstract public Object clone() throws CloneNotSupportedException;
}