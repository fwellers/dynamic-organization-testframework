package org.fwellers.helics_layer;

import java.util.EventListener;

public interface HelicsListener extends EventListener {
    void onStep(HelicsStepEvent helicsStepEvent);

    default String getFilter() {return "";}
}
