package org.fwellers.helics_layer;

public record ComplexNumber(double real, double imaginary) {
}
