package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import com.java.helics.helics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract class for java based federates.
 */
public abstract class Federate {
    private static final Logger logger = LoggerFactory.getLogger(Federate.class);
    protected SWIGTYPE_p_void federate;

    public abstract String getFederateName();

    // todo: Maybe throw exception on fail? Needs to be handled in subclass.
    public Optional<String> createFederateFromConfig(String configName) {
        logger.info("helicsJava");
        logger.info("HELICS Version: {}", helics.helicsGetVersion());
        URL resource = getClass().getClassLoader().getResource(configName);
        if (resource == null) {
            logger.error("Error: Config resource {} was not found!", configName);
            return Optional.empty();
        }

        federate = helics.helicsCreateCombinationFederateFromConfig(resource.getFile());
        String federateName = helics.helicsFederateGetName(federate);
        logger.info("Registered Federate {}", federateName);
        return Optional.of(federateName);
    }

    protected PublicationReferences getPublicationReferences() {
        int pubCount = helics.helicsFederateGetPublicationCount(federate);
        logger.info("num publications: {}", pubCount);

        List<SWIGTYPE_p_void> pubPointers = new ArrayList<>();
        for (int i = 0; i < pubCount; i++) {
            SWIGTYPE_p_void swigtypePVoid = helics.helicsFederateGetPublicationByIndex(federate, i);
            pubPointers.add(swigtypePVoid);
            String s = helics.helicsPublicationGetName(swigtypePVoid);
            logger.info("Publication: {}", s);
        }
        PublicationReferences publicationReferences = new PublicationReferences(pubCount, pubPointers);
        return publicationReferences;
    }

    protected record PublicationReferences(int pubCount, List<SWIGTYPE_p_void> pubPointers) {
    }

    protected SubscriptionReferences getSubscriptionReferences() {
        int subCount = helics.helicsFederateGetInputCount(federate);
        logger.info("num subscriptions: {}", subCount);
        List<SWIGTYPE_p_void> subPointers = new ArrayList<>();
        for (int i = 0; i < subCount; i++) {
            SWIGTYPE_p_void swigtypePVoid = helics.helicsFederateGetInputByIndex(federate, i);
            /*double d = 5;
            helics.helicsInputSetDefaultDouble(swigtypePVoid, d);*/
            String s = helics.helicsSubscriptionGetTarget(swigtypePVoid);
            subPointers.add(swigtypePVoid);
            logger.info("Subscriptions: {}", s);
        }
        SubscriptionReferences result = new SubscriptionReferences(subCount, subPointers);
        return result;
    }

    protected record SubscriptionReferences(int subCount, List<SWIGTYPE_p_void> subPointers) {
    }

    public void destroyFederate() {
        helics.helicsFederateDisconnect(federate);
        helics.helicsFederateDestroy(federate);
        logger.info("Destroying federate for Federate {}", getFederateName());
    }
}
