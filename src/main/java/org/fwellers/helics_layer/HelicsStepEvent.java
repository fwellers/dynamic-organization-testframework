package org.fwellers.helics_layer;

import java.util.EventObject;

public class HelicsStepEvent extends EventObject {
    private final transient AgentSubscriberResponse context;

    public HelicsStepEvent(Object source, AgentSubscriberResponse context) {
        super(source);
        this.context = context;
    }

    public AgentSubscriberResponse getContext() {
        return context;
    }
}
