package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import com.java.helics.helics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoubleAgentPublication extends AgentPublication {
    private static final Logger logger = LoggerFactory.getLogger(DoubleAgentPublication.class);
    private double value;

    public DoubleAgentPublication(int agentId, String target, Unit targetUnit, double value) {
        super(agentId, target, targetUnit);
        this.targetUnit = targetUnit;
        this.value = value;
    }

    public DoubleAgentPublication(int agentId, String target, Unit targetUnit) {
        super(agentId, target, targetUnit);
        this.value = -1;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void setValue(Object value, Unit temperatureUnit) {
        if (targetUnit == temperatureUnit) {
            this.value = (double) value;
        } else {
            // assume targetUnit=F and temperatureUnit=C, C -> F
            this.value = ((double) value * 1.8) + 32;
        }
    }

    @Override
    public boolean matchesPublication(String target) {
        return getFullTarget().equals(target);
    }

    @Override
    public void publishValue(SWIGTYPE_p_void publicationPointer) {
        helics.helicsPublicationPublishDouble(publicationPointer, value);
        logger.info("published value {} to target {}", value, fullTarget);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new DoubleAgentPublication(agentId, fullTarget, targetUnit, value);
    }

}
