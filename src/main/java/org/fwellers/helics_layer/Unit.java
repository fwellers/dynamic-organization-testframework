package org.fwellers.helics_layer;

public enum Unit {
    F, C, VA, V;


    @Override
    public String toString() {
        switch (this) {
            case C -> {
                return "C";
            }
            case F -> {
                return "F";
            }
            case VA -> {
                return "VA";
            }
            case V -> {
                return "V";
            }
            default -> {
                return "";
            }
        }
    }
}
