package org.fwellers.helics_layer;

//todo: change unit to type
public record HelicsSubscriptionInfo<T>(String target, String type, String unit, T value) implements Cloneable {
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new HelicsSubscriptionInfo<>(target, type, unit, value);
    }
}
