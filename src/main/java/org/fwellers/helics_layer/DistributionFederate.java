package org.fwellers.helics_layer;

import com.java.helics.helics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Optional;

public class DistributionFederate extends Federate implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(DistributionFederate.class);
    public DistributionFederate() {
        super();
    }

    @Override
    public void run() {
        logger.info("start run of federate {}", getFederateName());
        double hours = 2;
        double totalInterval = 60 * 60 * hours;
        double checkInterval = 5 * 60;
        double grantedTime = -1;


        Optional<String> federateName = createFederateFromConfig("helics_configs/distributionFederate.json");


        // Publication and Subscription references
        PublicationReferences publicationReferences = getPublicationReferences();
        SubscriptionReferences subscriptionReferences = getSubscriptionReferences();

        // Enter Execution Mode
        helics.helicsFederateEnterInitializingMode(federate);
        helics.helicsFederateEnterExecutingMode(federate);

        double t = 0;
        while (t < totalInterval) {

            // publishing values
            double myPubVal = 3.1415926 * t;
            for (int i = 0; i < publicationReferences.pubCount(); i++) {
                helics.helicsPublicationPublishDouble(publicationReferences.pubPointers().get(i), myPubVal);
                logger.info("publish value is {}", myPubVal);
            }

            // subscribing to values
            for (int i = 0; i < subscriptionReferences.subCount(); i++) {
                double v = helics.helicsInputGetDouble(subscriptionReferences.subPointers().get(i));
                logger.info("subscribe value is {}", v);
            }
            while (grantedTime < t) {
                grantedTime = helics.helicsFederateRequestTime(federate, t);
            }
            t = t + checkInterval;
        }

        destroyFederate();
    }



    @Override
    public String getFederateName() {
        return "Distribution Federate";
    }
}
