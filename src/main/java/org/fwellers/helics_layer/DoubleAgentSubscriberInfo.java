package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import com.java.helics.helics;
import jadex.future.IntermediateFuture;

import java.util.Objects;

public final class DoubleAgentSubscriberInfo implements AgentSubscriberInfo<Double> {
    private IntermediateFuture<AgentSubscriberResponse> listener;
    private final String target;
    private Double value;
    private Unit unit;

    public DoubleAgentSubscriberInfo(String target, Unit unit) {
        this.target = target;
        this.unit = unit;
        this.value = -1.0;
    }

    public DoubleAgentSubscriberInfo(IntermediateFuture<AgentSubscriberResponse> listener, String target, Unit unit) {
        this.listener = listener;
        this.target = target;
        this.unit = unit;
    }

    @Override
    public String getTarget() {
        return target;
    }

    @Override
    public Unit getUnit() {
        return unit;
    }

    @Override
    public void setListener(IntermediateFuture<AgentSubscriberResponse> listener) {
        this.listener = listener;
    }

    @Override
    public IntermediateFuture<AgentSubscriberResponse> getListener() {
        return listener;
    }

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (DoubleAgentSubscriberInfo) obj;
        return Objects.equals(this.target, that.target) &&
                Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(target, unit, value);
    }

    @Override
    public String toString() {
        return "DoubleAgentSubscriberInfo[" +
                "target=" + target + ", " +
                "unit=" + unit + ", " +
                "value=" + value + ']';
    }

    @Override
    public boolean acceptsSubscription(SWIGTYPE_p_void subRef) {
        return helics.helicsInputGetTarget(subRef).equals(target);
    }

    @Override
    public HelicsSubscriptionInfo<Double> parseSubscription(SWIGTYPE_p_void subRef) {
        if (unit.equals(Unit.F)) {
            return parseSubscription(subRef, true);
        } else {
            return parseSubscription(subRef, false);
        }
    }

    @Override
    public HelicsSubscriptionInfo<Double> parseSubscription(SWIGTYPE_p_void subRef, boolean convertToCelsius) {
        String type = helics.helicsInputGetType(subRef);
        String target = helics.helicsInputGetTarget(subRef);

        if (type.equals("double")) {
            if (convertToCelsius && Unit.F.equals(unit)) {
                double fahrenheit = helics.helicsInputGetDouble(subRef);
                double celsius = ((fahrenheit-32) * 5) / 9;
                return new HelicsSubscriptionInfo<>(target, type, Unit.C.toString(), celsius);
            } else {
                return new HelicsSubscriptionInfo<>(target, type, unit.toString(), helics.helicsInputGetDouble(subRef));
            }
        } else {
            throw new IllegalStateException("Unexpected value: " + type);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleAgentSubscriberInfo doubleAgentSubscriberInfo = new DoubleAgentSubscriberInfo(listener, target, unit);
        doubleAgentSubscriberInfo.setValue(value);
        return doubleAgentSubscriberInfo;
    }
}
