package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import com.java.helics.helics;
import jadex.core.IComponent;
import jadex.execution.IExecutionFeature;
import jadex.future.*;
import jadex.micro.annotation.Agent;
import jadex.providedservice.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Agent
@Service
public class HelicsFederateSchedulerServiceImpl extends Federate implements StepSchedulerService {
    private static final Logger logger = LoggerFactory.getLogger(HelicsFederateSchedulerServiceImpl.class);
    //todo: generic might not be best for this!
    protected List<AgentSubscriberInfo> subscriberList = new ArrayList<>();

    List<AgentPublication> responses = new ArrayList<>();
    private PublicationReferences publicationReferences;
    private SubscriptionReferences subscriptionReferences;
    private double currentTime = 0;
    private double grantedTime = -1;
    private String federateName;

    // Configuration Parameters
    private final int expectedSubscribers;
    private final int expectedResponses;
    private final double hours = 24;
    private final double totalInterval = 60 * 60 * hours;
    private final double checkInterval = 5 * 60;

    public HelicsFederateSchedulerServiceImpl(int expectedSubscribers, int expectedResponses) {
        this.expectedSubscribers = expectedSubscribers;
        this.expectedResponses = expectedResponses;
        this.federateName = "MultiAgentSim";
        logger.info("constructor for {} called", federateName);
    }

    @Agent
    protected IComponent agent;

    @Override
    public <T> IntermediateFuture<AgentSubscriberResponse> subscribe(AgentSubscriberInfo<T> agentSubscriberInfo) {
        logger.info("Subscribe agent.");
        IntermediateFuture<AgentSubscriberResponse> agentSubscriberResponseFuture = new IntermediateFuture<>();
        agentSubscriberInfo.setListener(agentSubscriberResponseFuture);
        subscriberList.add(agentSubscriberInfo);
        if (subscriberList.size() == expectedSubscribers) {
            agent.getFeature(IExecutionFeature.class).scheduleStep(a -> {
                initSim();
                beginStep();
                return IFuture.DONE;
            });
        }
        return agentSubscriberInfo.getListener();
    }

    @Override
    public IFuture<Void> sendResponse(AgentPublication agentPublication) {
        // optimistically add without checking if duplicates are used.
        responses.add(agentPublication);
        if (responses.size() == expectedResponses) {
            handlePublications();
        }
        return IFuture.DONE;
    }

    private void initSim() {
        grantedTime = -1;

        Optional<String> optionalFederateName = createFederateFromConfig("helics_configs/multiAgentFederate.json");
        if (optionalFederateName.isPresent()) {
            publicationReferences = getPublicationReferences();
            subscriptionReferences = getSubscriptionReferences();

            logger.info("Start Federate {}", optionalFederateName.get());
            this.federateName = optionalFederateName.get();
            helics.helicsFederateEnterInitializingMode(federate);
            helics.helicsFederateEnterExecutingMode(federate);
            currentTime = 0;
        } else {
            logger.error("Federate could not be created!");
        }

    }

    private void beginStep() {
        if (currentTime < totalInterval) {
            currentTime += checkInterval;

            while (grantedTime < currentTime) {
                grantedTime = helics.helicsFederateRequestTime(federate, currentTime);
            }

            for (AgentSubscriberInfo agentSubscriberInfo : subscriberList) {
                for (int i = 0; i < subscriptionReferences.subCount(); i++) {
                    SWIGTYPE_p_void subRef = subscriptionReferences.subPointers().get(i);
                    if (agentSubscriberInfo.acceptsSubscription(subRef)) {
                        HelicsSubscriptionInfo helicsSubscriptionInfo = agentSubscriberInfo.parseSubscription(subRef);
                        logger.info("Notify Subscriber at step {} with value {}", currentTime, helicsSubscriptionInfo.value());
                        AgentSubscriberResponse agentSubscriberResponse = new AgentSubscriberResponse(currentTime, helicsSubscriptionInfo);
                        IntermediateFuture<AgentSubscriberResponse> listener = agentSubscriberInfo.getListener();
                        listener.addIntermediateResult(agentSubscriberResponse);
                    }
                }
            }
        } else {
            endSim();
        }
    }

    private Optional<AgentPublication> getPublicationForTarget(String pubName) {
        for (AgentPublication response : responses) {
            if (response.matchesPublication(pubName)) {
                return Optional.of(response);
            }
        }
        return Optional.empty();
    }

    private void handlePublications() {
        logger.debug("Sending publications.");
        for (int i = 0; i < publicationReferences.pubCount(); i++) {
            String pubName = helics.helicsPublicationGetName(publicationReferences.pubPointers().get(i));
            Optional<AgentPublication> optionalPublication = getPublicationForTarget(pubName);
            if (optionalPublication.isPresent()) {
                optionalPublication.get().publishValue(publicationReferences.pubPointers().get(i));
            }
        }
        responses.clear();
        beginStep();
    }

    private void endSim() {
        destroyFederate();
    }

    @Override
    public String getFederateName() {
        return federateName;
    }
}
