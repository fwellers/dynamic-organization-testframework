package org.fwellers.helics_layer;

import java.io.Serial;
import java.io.Serializable;

public class AgentSubscriberResponse implements Cloneable, Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private final double timestamp;
    private final HelicsSubscriptionInfo<Double> helicsResponse;

    public AgentSubscriberResponse(double timestamp, HelicsSubscriptionInfo<Double> helicsSubscriptionInfo) {
        this.timestamp = timestamp;
        this.helicsResponse = helicsSubscriptionInfo;
    }

    public double timestamp() {
        return timestamp;
    }

    public HelicsSubscriptionInfo<Double> getHelicsResponse() {
        return helicsResponse;
    }

    public Double getValue() {
        return helicsResponse.value();
    }

    public String getTarget() {
        return helicsResponse.target();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new AgentSubscriberResponse(timestamp, helicsResponse);
    }
}
