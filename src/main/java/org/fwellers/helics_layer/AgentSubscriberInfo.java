package org.fwellers.helics_layer;

import com.java.helics.SWIGTYPE_p_void;
import jadex.future.IntermediateFuture;


public interface AgentSubscriberInfo<T> extends Cloneable {
    String getTarget();

    T getValue();

    Unit getUnit();

    void setListener(IntermediateFuture<AgentSubscriberResponse> listener);

    IntermediateFuture<AgentSubscriberResponse> getListener();

    void setValue(T newVal);

    boolean acceptsSubscription(SWIGTYPE_p_void subRef);

    HelicsSubscriptionInfo<T> parseSubscription(SWIGTYPE_p_void subRef, boolean convertToCelsius);

    /**
     * Same as {@link AgentSubscriberInfo#parseSubscription(SWIGTYPE_p_void, boolean)} with convertToCelsius=true as
     * default.
     *
     * @param subRef
     * @return
     */
    HelicsSubscriptionInfo<T> parseSubscription(SWIGTYPE_p_void subRef);

}