package org.fwellers;

import jadex.core.IComponent;
import org.fwellers.agents.EnvironmentService;
import org.fwellers.agents.EnvironmentServiceProvider;
import org.fwellers.agents.MainSystemAgent;
import org.fwellers.communication.CommunicationService;
import org.fwellers.communication.CommunicationServiceProvider;
import org.fwellers.configuration.ConfigurationProvider;
import org.fwellers.helics_layer.DistributionFederate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static final boolean USE_JAVA_SIMULATION = false;

    public static void main(String[] args) {
        logger.info("Starting Simulation");

        ConfigurationProvider configurationProvider = new ConfigurationProvider();
        EnvironmentService environmentService = new EnvironmentServiceProvider();
        CommunicationService communicationService = new CommunicationServiceProvider();

        IComponent.create(configurationProvider);
        IComponent.create(environmentService);
        IComponent.create(communicationService);

        if (USE_JAVA_SIMULATION) {
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            DistributionFederate distributionFederate = new DistributionFederate();
            executorService.execute(distributionFederate);

            IComponent.create(new MainSystemAgent());

            IComponent.waitForLastComponentTerminated();
            executorService.shutdown();
        } else {

            IComponent.create(new MainSystemAgent());

            IComponent.waitForLastComponentTerminated();
        }

        logger.info("Finished Simulation");
    }
}