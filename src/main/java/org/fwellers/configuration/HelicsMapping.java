package org.fwellers.configuration;

import org.fwellers.helics_layer.Unit;

public record HelicsMapping(String target, Unit unit) {
}
