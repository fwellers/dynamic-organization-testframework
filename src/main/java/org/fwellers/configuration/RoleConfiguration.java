package org.fwellers.configuration;

import java.util.Map;

public record RoleConfiguration(String name, Map<String, String> properties) {
}
