package org.fwellers.configuration;

import jadex.providedservice.annotation.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service for providing system-wide configuration.
 */
@Service
public interface ConfigurationService {
    /**
     * Query the configurations for all agents in the system.
     *
     * @return A List of {@link AgentConfiguration}s.
     */
    List<AgentConfiguration> getAgents();

    /**
     * Query the number of total publications provided by the helics federate.
     *
     * @return The total number of publications provided.
     */
    int totalPublications();

}
