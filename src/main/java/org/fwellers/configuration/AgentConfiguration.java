package org.fwellers.configuration;

import org.fwellers.agents.EnvironmentService;
import org.fwellers.communication.CommunicationService;
import org.fwellers.communication.MessageReceivedEventListener;
import org.fwellers.helics_layer.AgentPublication;
import org.fwellers.helics_layer.DoubleAgentPublication;
import org.fwellers.roles.FeederRole;
import org.fwellers.roles.AdaptiveHouseHeatingRole;
import org.fwellers.roles.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public record AgentConfiguration(String name, List<String> groups, RoleConfiguration role, String OMA,
                                 List<HelicsMapping> subscriptionTargets, List<HelicsMapping> publicationTargets) {
    private static final Logger logger = LoggerFactory.getLogger(AgentConfiguration.class);

    public Role<? extends MessageReceivedEventListener> roleForConfig(int id, EnvironmentService environmentService, CommunicationService communicationService) {
        String roleName = role().name();
        List<AgentPublication> publicationInfoList = new ArrayList<>();
        publicationTargets().stream()
                .map(target -> new DoubleAgentPublication(id, target.target(), target.unit()))
                .forEach(publicationInfoList::add);
        Role<? extends MessageReceivedEventListener> role = new AdaptiveHouseHeatingRole(publicationInfoList, communicationService, this);
        if (AdaptiveHouseHeatingRole.class.getSimpleName().equals(roleName)) {
            role = new AdaptiveHouseHeatingRole(publicationInfoList, communicationService, this);
        } else if (FeederRole.class.getSimpleName().equals(roleName)) {
            role = new FeederRole(environmentService, communicationService, this);
        } else {
            logger.error("Error in role mapping, no implementation found for role {}.", roleName);
        }
        return role;
    }
}
