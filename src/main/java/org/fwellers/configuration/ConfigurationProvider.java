package org.fwellers.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import jadex.core.IComponent;
import jadex.micro.annotation.Agent;
import jadex.model.annotation.OnStart;
import jadex.providedservice.annotation.Service;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@Agent
@Service
public class ConfigurationProvider implements ConfigurationService {

    @Agent
    private IComponent agent;

    private MultiAgentSystemConfiguration multiAgentSystemConfiguration;

    @OnStart
    public void body() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);

        try {
            URL maConfig = getClass().getClassLoader().getResource("multiAgentSystemConfiguration.json");
            multiAgentSystemConfiguration = objectMapper.readValue(maConfig, MultiAgentSystemConfiguration.class);
            AgentConfiguration agentConfiguration = multiAgentSystemConfiguration.agents().get(0);
            String name = agentConfiguration.name();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<AgentConfiguration> getAgents() {
        return multiAgentSystemConfiguration.agents();
    }

    @Override
    public int totalPublications() {
        return multiAgentSystemConfiguration.agents().stream()
                .map(ac -> ac.publicationTargets().size())
                .reduce(0, Integer::sum);
    }
}
