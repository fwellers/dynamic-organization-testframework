package org.fwellers.configuration;

import java.util.List;

public record MultiAgentSystemConfiguration(List<AgentConfiguration> agents) {
}
