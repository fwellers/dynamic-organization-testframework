package org.fwellers.roles;

import java.util.EventObject;

public abstract class RoleEvent extends EventObject {
    protected RoleEvent(Object source) {
        super(source);
    }
}
