package org.fwellers.roles;

import jadex.future.IFuture;

public interface RoleListener {
    default IFuture<Void> onEvent(RoleEvent roleEvent) {
        return IFuture.DONE;}

    default IFuture<Void> onEvent(ReTriggerEvent event) {
        return IFuture.DONE;}
}
