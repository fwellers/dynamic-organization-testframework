package org.fwellers.roles;

import jadex.future.IFuture;
import org.fwellers.communication.*;
import org.fwellers.configuration.AgentConfiguration;
import org.fwellers.helics_layer.AgentPublication;
import org.fwellers.helics_layer.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class AdaptiveHouseHeatingRole implements Role<AdaptiveHouseHeatingRole> {
    private static final Logger logger = LoggerFactory.getLogger(AdaptiveHouseHeatingRole.class);

    //todo: change type of property keys to enum to enable multiple properties
    public static final String AIR_TEMPERATURE_PROPERTY = "airTemperature";
    public static final String HELICS_COOLING_SETPOINT_PROPERTY = "cooling_setpoint";
    public static final String HELICS_HEATING_SETPOINT_PROPERTY = "heating_setpoint";

    private double airTemperature;
    private static final int NUM_EXPECTED_VALUES = 2;
    private final List<AgentPublication> publications;
    private final CommunicationService communicationService;
    private final AgentConfiguration agentConfiguration;

    private double coolingSetpoint;
    private double targetCoolingSetpoint;
    private double heatingSetpoint;
    private double targetHeatingSetpoint;
    private double maxPreferredStepSize;

    private Map<String, List<String>> negotiationMembers = new HashMap<>();
    private List<Suggestion> suggestions = new ArrayList<>();
    private Map<String, Integer> negotiationTimes = new HashMap<>();

    private int receivedValues;
    private RoleListener roleListener;
    private int currentConversations;

    public AdaptiveHouseHeatingRole(List<AgentPublication> publications, CommunicationService communicationService, AgentConfiguration agentConfiguration) {
        this.publications = publications;
        this.communicationService = communicationService;
        this.agentConfiguration = agentConfiguration;
        this.receivedValues = 0;
        Map<String, String> properties = agentConfiguration.role().properties();
        this.targetCoolingSetpoint = Integer.parseInt(properties.getOrDefault("targetCoolingSetpoint","0"));
        this.coolingSetpoint = this.targetCoolingSetpoint;
        this.targetHeatingSetpoint = Integer.parseInt(properties.getOrDefault("targetHeatingSetpoint","0"));
        this.heatingSetpoint = this.targetHeatingSetpoint;
        this.maxPreferredStepSize = Double.parseDouble(properties.getOrDefault("maxPreferredStepSize", "1"));
        this.currentConversations = 0;
    }

    @Override
    public void setListener(RoleListener roleListener) {
        this.communicationService.registerListener(this);
        this.roleListener = roleListener;
    }

    @Override
    public boolean isReady() {
        return NUM_EXPECTED_VALUES == this.receivedValues;
    }

    @Override
    public List<AgentPublication> execute() {
        this.receivedValues = 0;
        List<AgentPublication> resultList = new ArrayList<>();
        //todo: make mapping decision more complex
        //communicationService.sendMessage(new MessageEvent<>(this, Role.class, FeederMessage.TOO_HIGH, agentConfiguration.name()));
        for (AgentPublication agentPublication : publications) {
            String target = agentPublication.getTargetProperty();
            if (HELICS_COOLING_SETPOINT_PROPERTY.equals(target)) {
                agentPublication.setValue(coolingSetpoint, Unit.C);
            } else if (HELICS_HEATING_SETPOINT_PROPERTY.equals(target)) {
                agentPublication.setValue(heatingSetpoint, Unit.C);
            } else {
                logger.error("Invalid publication target {} registered in NaiveHouseHeatingRule.", target);
            }
            resultList.add(agentPublication);
        }
        return resultList;
    }

    @Override
    public List<AgentPublication> getPublications() {
        return publications;
    }

    @Override
    public <T> void setProperty(String propertyName, T value) {
        if (AIR_TEMPERATURE_PROPERTY.equals(propertyName) && value instanceof Double doubleVal) {
            airTemperature = doubleVal;
            this.receivedValues++;
        } else {
            //todo: throw exception
            logger.error("Property target or type mismatch.");
        }
    }


    @Override
    public boolean matches(FilterValue filter) {
        String name = agentConfiguration.name();
        if (name.length() != 4) {
            return false;
        }
        char branchNum = name.charAt(1);
        return branchNum == filter.branch();
    }

    private Optional<Suggestion> getSuggestion(String agentName, String property, int time) {
        return suggestions.stream()
                .filter(suggestion -> suggestion.time() == time)
                .filter(suggestion -> suggestion.agentName().equals(agentName))
                .filter(suggestion -> suggestion.property().equals(property))
                .findFirst();
    }

    private void increaseNegotiationTime(String property) {
        negotiationTimes.computeIfPresent(property, (string, integer) -> integer + 1);
    }

    private void addSuggestion(Suggestion suggestion) {
        Optional<Suggestion> alreadySaved = suggestions.stream()
                .filter(s -> s.agentName().equals(suggestion.agentName()))
                .filter(s -> s.property().equals(suggestion.property()))
                .findAny();
        alreadySaved.ifPresent(value -> suggestions.remove(value));
        suggestions.add(suggestion);
    }

    private long getNumSuggestions(String property, int time) {
        return suggestions.stream()
                .filter(suggestion -> suggestion.time() == time)
                .filter(suggestion -> suggestion.property().equals(property))
                .count();
    }

    private int getNumNegotiationMembers(String property) {
        if (negotiationMembers.containsKey(property)) {
            return negotiationMembers.get(property).size();
        } else {
            return 0;
        }
    }

    private void removeNegotiationMember(String property, String agentName) {
        List<String> strings = negotiationMembers.get(property);
        strings.remove(agentName);
    }

    private void addNegotiationMember(String property, String agentName) {
        if (negotiationMembers.containsKey(property)) {
            List<String> strings = negotiationMembers.get(property);
            if (!strings.contains(agentName)) {
                strings.add(agentName);
            }
        } else {
            ArrayList<String> agentNames = new ArrayList<>();
            agentNames.add(agentName);
            negotiationMembers.put(property, agentNames);
        }
    }

    private Optional<Double> getMajorityValue(String property) {
        Map<Double, Integer> occurrences = new HashMap<>();
        List<Suggestion> propertyMatchingSuggestions = suggestions.stream()
                .filter(suggestion -> suggestion.time() == negotiationTimes.get(property))
                .filter(suggestion -> suggestion.property().equals(property))
                .toList();
        for (Suggestion suggestion : propertyMatchingSuggestions) {
            boolean exists = false;
            for (Map.Entry<Double, Integer> entry : occurrences.entrySet()) {
                double diff = entry.getKey() - suggestion.value();
                double precision = 0.2;
                if (diff < precision) {
                    exists = true;
                    occurrences.computeIfPresent(entry.getKey(), (k, valFrequency) -> valFrequency + 1);
                }
            }
            if (!exists) {
                occurrences.put(suggestion.value(), 1);
            }
        }
        double maxVal = 0;
        int maxFrequency = 0;
        for (Map.Entry<Double, Integer> entry : occurrences.entrySet()) {
            Double value = entry.getKey();
            Integer frequency = entry.getValue();
            if (frequency > maxFrequency) {
                maxVal = value;
                maxFrequency = frequency;
            }
        }
        if (maxFrequency > propertyMatchingSuggestions.size() / 2.0) {
            return Optional.of(maxVal);
        } else {
            return Optional.empty();
        }
    }

    private double getAverageSuggestion(String property, int time) {
        List<Double> list = suggestions.stream()
                .filter(suggestion -> suggestion.property().equals(property))
                .filter(suggestion -> suggestion.time() == time)
                .map(Suggestion::value)
                .toList();

        Optional<Double> optionalAverage = list.stream().reduce(Double::sum).map(sum -> sum / list.size());
        if (optionalAverage.isEmpty()) {
            logger.info("Average could not be calculated.");
            return 0;
        } else {
            return optionalAverage.get();
        }

    }

    @Override
    public IFuture<Void> onMessage(MessageEvent<AdaptiveHouseHeatingRole> messageEvent) {
        if (messageEvent.getMessage() instanceof FeederMessage feederMessage && feederMessage == FeederMessage.TOO_HIGH) {
            /*addNegotiationMember(HELICS_COOLING_SETPOINT_PROPERTY, agentConfiguration.name());
            addNegotiationMember(HELICS_HEATING_SETPOINT_PROPERTY, agentConfiguration.name());*/
            communicationService.initNegotiation(new InitNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                    new NegotiationInitializeMessage(agentConfiguration.name()), agentConfiguration.name()));
            //do nothing, since neither heating nor cooling are used and there are no options to reduce
        } else if (messageEvent.getMessage() instanceof FeederMessage feederMessage && feederMessage == FeederMessage.OK) {
            this.receivedValues++;
            if (airTemperature < coolingSetpoint && coolingSetpoint > heatingSetpoint + 1 && coolingSetpoint > targetCoolingSetpoint) {
                coolingSetpoint = coolingSetpoint - 1;
            }
            if (airTemperature > heatingSetpoint && heatingSetpoint < coolingSetpoint - 1 && heatingSetpoint < targetHeatingSetpoint) {
                heatingSetpoint = heatingSetpoint + 1;
            }
        }
        if (isReady()) {
            roleListener.onEvent(new ReTriggerEvent(this));
        }
        return IFuture.DONE;
    }

    @Override
    public IFuture<Void> onMessage(InitNegotiationBody<AdaptiveHouseHeatingRole> initNegotiationBody) {
        logger.info("Agent {} init negotiation source {}.", agentConfiguration.name(), initNegotiationBody.getMessage().agentName());
        /*if (initNegotiationBody.getMessage().agentName().equals(agentConfiguration.name())) {
            return IFuture.DONE;
        }*/
        //todo: make it work with multiple concurrent discussions, list of names is not enough
        addNegotiationMember(HELICS_COOLING_SETPOINT_PROPERTY, initNegotiationBody.getMessage().agentName());
        addNegotiationMember(HELICS_HEATING_SETPOINT_PROPERTY, initNegotiationBody.getMessage().agentName());
        negotiationTimes.put(HELICS_COOLING_SETPOINT_PROPERTY, 0);
        negotiationTimes.put(HELICS_HEATING_SETPOINT_PROPERTY, 0);
        //todo: more generic, one should be enough though
        if (getNumNegotiationMembers(HELICS_COOLING_SETPOINT_PROPERTY) == initNegotiationBody.getMaxParticipants()) {
            if (airTemperature > coolingSetpoint) {
                Suggestion onwInitialCoolingSuggestion = new Suggestion(agentConfiguration.name(), HELICS_COOLING_SETPOINT_PROPERTY, ThreadLocalRandom.current().nextDouble(1, maxPreferredStepSize), 0);
                //addSuggestion(onwInitialCoolingSuggestion);
                //addNegotiationMember(HELICS_COOLING_SETPOINT_PROPERTY, agentConfiguration.name());
                this.currentConversations++;
                communicationService.proposeAction(new ProposeNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiateActionMessage<>(negotiationTimes.get(HELICS_COOLING_SETPOINT_PROPERTY), agentConfiguration.name(),
                                HELICS_COOLING_SETPOINT_PROPERTY, onwInitialCoolingSuggestion.value()), negotiationTimes.get(HELICS_COOLING_SETPOINT_PROPERTY), agentConfiguration.name()));
            } else {
                Suggestion onwInitialCoolingSuggestion = new Suggestion(agentConfiguration.name(), HELICS_COOLING_SETPOINT_PROPERTY, 0d, 0);
                //addSuggestion(onwInitialCoolingSuggestion);
                this.currentConversations++;
                communicationService.proposeAction(new ProposeNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiateActionMessage<>(negotiationTimes.get(HELICS_COOLING_SETPOINT_PROPERTY), agentConfiguration.name(),
                                HELICS_COOLING_SETPOINT_PROPERTY, onwInitialCoolingSuggestion.value()), negotiationTimes.get(HELICS_COOLING_SETPOINT_PROPERTY), agentConfiguration.name()));
            }
        }
        if (getNumNegotiationMembers(HELICS_HEATING_SETPOINT_PROPERTY) == initNegotiationBody.getMaxParticipants()) {
            if (airTemperature < heatingSetpoint) {
                Suggestion ownInitialHeatingSuggestion = new Suggestion(agentConfiguration.name(), HELICS_HEATING_SETPOINT_PROPERTY, -ThreadLocalRandom.current().nextDouble(1, maxPreferredStepSize), 0);
                //addSuggestion(ownInitialHeatingSuggestion);
                //addNegotiationMember(HELICS_HEATING_SETPOINT_PROPERTY, agentConfiguration.name());
                this.currentConversations++;
                communicationService.proposeAction(new ProposeNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiateActionMessage<>(negotiationTimes.get(HELICS_HEATING_SETPOINT_PROPERTY), agentConfiguration.name(),
                                HELICS_HEATING_SETPOINT_PROPERTY, ownInitialHeatingSuggestion.value()), negotiationTimes.get(HELICS_HEATING_SETPOINT_PROPERTY), agentConfiguration.name()));
            } else {
                Suggestion ownInitialHeatingSuggestion = new Suggestion(agentConfiguration.name(), HELICS_HEATING_SETPOINT_PROPERTY, 0d, 0);
                //addSuggestion(ownInitialHeatingSuggestion);

                this.currentConversations++;
                communicationService.proposeAction(new ProposeNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiateActionMessage<>(negotiationTimes.get(HELICS_HEATING_SETPOINT_PROPERTY), agentConfiguration.name(),
                                HELICS_HEATING_SETPOINT_PROPERTY, ownInitialHeatingSuggestion.value()), negotiationTimes.get(HELICS_HEATING_SETPOINT_PROPERTY), agentConfiguration.name()));
            }
        }
        return IFuture.DONE;
    }

    @Override
    public IFuture<Void> onMessage(ProposeNegotiationBody<AdaptiveHouseHeatingRole, Double> proposeNegotiationBody) {
        NegotiateActionMessage<Double> message = proposeNegotiationBody.getMessage();
        String messageProperty = message.propertyName();
        Double messageDelta = message.delta();
        addSuggestion(new Suggestion(message.sourceAgentName(), messageProperty, messageDelta, message.time()));

        logger.info("Agent {} receive proposal {} nr {} from {} property {}.",
                agentConfiguration.name(),
                proposeNegotiationBody.getMessage().delta(),
                getNumSuggestions(messageProperty, negotiationTimes.get(messageProperty)),
                proposeNegotiationBody.getMessage().sourceAgentName(),
                messageProperty);


        long numSuggestions = getNumSuggestions(messageProperty, negotiationTimes.get(messageProperty));
        int numNegotiationMembers = getNumNegotiationMembers(messageProperty);
        if (numSuggestions == numNegotiationMembers && numSuggestions > 0) {
            Optional<Double> majorityValue = getMajorityValue(messageProperty);
            if (majorityValue.isPresent()) {
                logger.info("{} from {} with {} negotiation members and messageProperty {}:{} at Timestep {} finished",
                        agentConfiguration.name(), proposeNegotiationBody.getMessage().sourceAgentName(),
                        numNegotiationMembers, messageProperty,
                        majorityValue.get(), negotiationTimes.get(messageProperty));
                if (messageProperty.equals(HELICS_COOLING_SETPOINT_PROPERTY)) {
                    coolingSetpoint = coolingSetpoint + majorityValue.get();
                }
                if (messageProperty.equals(HELICS_HEATING_SETPOINT_PROPERTY)) {
                    heatingSetpoint = heatingSetpoint + majorityValue.get();
                }
                communicationService.finishNegotiation(new FinishNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiationFinishMessage(agentConfiguration.name(), messageProperty), agentConfiguration.name()));
            } else {
                // make new suggestion
                Suggestion suggestion = new Suggestion(agentConfiguration.name(), messageProperty,
                        getAverageSuggestion(messageProperty, negotiationTimes.get(messageProperty)), negotiationTimes.get(messageProperty) + 1);
                logger.info("{} from {} with {} negotiation members and messageProperty {}:{} at Timestep {} continued",
                        agentConfiguration.name(), proposeNegotiationBody.getMessage().sourceAgentName(),
                        numNegotiationMembers, messageProperty,
                        suggestion.value(), negotiationTimes.get(messageProperty));
                increaseNegotiationTime(messageProperty);
                addSuggestion(suggestion);
                communicationService.proposeAction(new ProposeNegotiationBody<>(this, AdaptiveHouseHeatingRole.class,
                        new NegotiateActionMessage<>(negotiationTimes.get(messageProperty), agentConfiguration.name(),
                                messageProperty, suggestion.value()), negotiationTimes.get(messageProperty), agentConfiguration.name()));
            }
        }
        return IFuture.DONE;
    }

    @Override
    public IFuture<Void> onMessage(FinishNegotiationBody<AdaptiveHouseHeatingRole> finishNegotiationBody) {
        removeNegotiationMember(finishNegotiationBody.getMessage().propertyName(), finishNegotiationBody.getMessage().sourceAgentName());
        if (getNumNegotiationMembers(finishNegotiationBody.getMessage().propertyName()) == 0) {
            logger.info("Agent {} finished negotiation for property {}.", agentConfiguration.name(), finishNegotiationBody.getMessage().propertyName());
            //printSuggestions(finishNegotiationBody.getMessage().propertyName());
            currentConversations--;
        }
        if (currentConversations == 0) {
            this.receivedValues++;
            negotiationTimes = new HashMap<>();
            suggestions = new ArrayList<>();
            negotiationMembers = new HashMap<>();
            logger.info("All negotiations from agent {} successful.", agentConfiguration.name());
        }
        if (isReady()) {
            roleListener.onEvent(new ReTriggerEvent(this));
        }
        return IFuture.DONE;
    }

    @Override
    public boolean filterSet() {
        return true;
    }

    private void printSuggestions(String property) {
        StringBuilder stringBuilder = new StringBuilder();
        suggestions.stream()
                .filter(suggestion -> suggestion.property().equals(property))
                .forEach(suggestion -> stringBuilder.append("value: ").append(suggestion.value()).append(" time: ").append(suggestion.time()).append("\n"));

        if (logger.isInfoEnabled()) {
            logger.info("Agent {} has suggestions {}.", agentConfiguration.name(), stringBuilder);
        }
    }
}
