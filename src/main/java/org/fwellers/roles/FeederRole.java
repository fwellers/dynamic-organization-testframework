package org.fwellers.roles;

import jadex.future.IFuture;
import org.fwellers.agents.EnvironmentService;
import org.fwellers.communication.CommunicationService;
import org.fwellers.communication.FeederMessage;
import org.fwellers.communication.MessageEvent;
import org.fwellers.configuration.AgentConfiguration;
import org.fwellers.helics_layer.AgentPublication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class FeederRole implements Role<FeederRole> {
    private static final Logger logger = LoggerFactory.getLogger(FeederRole.class);

    public static final String TOTAL_LOAD_PROPERTY = "totalLoad";

    private final EnvironmentService environmentService;
    private final CommunicationService communicationService;
    private final AgentConfiguration agentConfiguration;

    private double totalLoad;

    private static final int NUM_EXPECTED_VALUES = 1;

    private int receivedValues = 0;
    private RoleListener roleListener;

    public FeederRole(EnvironmentService environmentService, CommunicationService communicationService, AgentConfiguration agentConfiguration) {
        this.environmentService = environmentService;
        this.communicationService = communicationService;
        this.agentConfiguration = agentConfiguration;
    }

    @Override
    public List<AgentPublication> execute() {
        this.receivedValues = 0;
        double realPowerThreshold = Double.parseDouble(agentConfiguration.role().properties().
                getOrDefault("threshold", String.valueOf(environmentService.getFeederThreshold())));
        if (totalLoad > realPowerThreshold) {
            logger.info("Feeder Load {} is above Threshold.", totalLoad);
            communicationService.sendMessage(new MessageEvent<>(this, AdaptiveHouseHeatingRole.class, FeederMessage.TOO_HIGH));
        } else {
            communicationService.sendMessage(new MessageEvent<>(this, AdaptiveHouseHeatingRole.class, FeederMessage.OK));
        }
        return Collections.emptyList();
    }

    @Override
    public List<AgentPublication> getPublications() {
        return Collections.emptyList();
    }

    @Override
    public <T> void setProperty(String propertyName, T value) {
        if (TOTAL_LOAD_PROPERTY.equals(propertyName) && value instanceof Double number) {
            totalLoad = number;
            this.receivedValues++;
        } else {
            //todo: throw exception
            logger.error("Property target or type mismatch.");
        }
    }

    @Override
    public void setListener(RoleListener roleListener) {
        this.roleListener = roleListener;
    }

    @Override
    public boolean isReady() {
        return NUM_EXPECTED_VALUES == this.receivedValues;
    }

    @Override
    public IFuture<Void> onMessage(MessageEvent<FeederRole> messageEvent) {
        return IFuture.DONE;
    }

}
