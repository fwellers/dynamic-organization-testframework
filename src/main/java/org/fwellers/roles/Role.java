package org.fwellers.roles;

import org.fwellers.communication.MessageReceivedEventListener;
import org.fwellers.helics_layer.AgentPublication;

import java.util.List;

/**
 * Interface for roles that agents can adopt. The roles can be the target of messages.
 * Implementing classes should use constants for their required properties, to enable
 * easier configuration from the implementing agents.
 *
 * @param <T> The type of the implementing class.
 */
public interface Role<T extends MessageReceivedEventListener<T>> extends MessageReceivedEventListener<T> {
    void setListener(RoleListener roleListener);

    /**
     * Check if the role is ready for computation.
     *
     * @return true, if the role has all values required for computation by {@link Role#execute()}, false otherwise.
     */
    boolean isReady();

    /**
     * Compute the values for the publications.
     *
     * @return A list of computed {@link AgentPublication}s.
     */
    List<AgentPublication> execute();

    /**
     * Retrieve all the publications the role uses.
     *
     * @return A list of {@link AgentPublication}s the role supplies.
     */
    List<AgentPublication> getPublications();

    /**
     * Set a property of the role. The properties are required by the role to begin execution.
     * For the property names, the constants defined within the implementing classes should be used.
     *
     * @param propertyName The name of the property within the role.
     * @param value The new value to use.
     * @param <K> The type of the property.
     */
    <K> void setProperty(String propertyName, K value);
}
