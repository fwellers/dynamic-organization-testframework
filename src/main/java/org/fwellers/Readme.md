# General Discussion
What simulation level do we want? Household level? Or neighbourhood level?

# Files necessary:

1. Gridlab-D simulation files
   * glm file for system
   * tym3 files for climate data
   * (potential) schedule files for electrical appliances
2. Helics config
   * co-simulation runner (potentially optional)
   * Configuration for java multi agent system federate
   * Configuration for Gridlab-D federate 


## Gridlab-D
Different modeling levels, model houses in Gridlabd and influence aspects via Multi-Agent system

## HELICS
### Gridlab-D Federate
* Define the publications and subscriptions
 
### Java Federate
* Define the publications and subscriptions


Each Role has several inputs (HELICS Subscriptions) and outputs (HELICS Publications)