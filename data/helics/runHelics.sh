#!/bin/bash

#(exec helics_broker -f 2 --loglevel=warning --name=mainbroker &> broker.log &)
(exec helics_broker -f 2 --loglevel=debug &)
#(exec helics_recorder --config-file helicsRecorder.json --timedelta 1s --period 1s --stop 21600s &> tracer.log &)
#(exec gridlabd -D WANT_HELICS_NO_NS3 ../gridlab-d/basic_example.glm &> gridlabd.log &)
(exec gridlabd -D WANT_HELICS_NO_NS3 ../gridlab-d/house_line_example.glm &)
